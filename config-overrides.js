const { override, fixBabelImports, addLessLoader } = require('customize-cra');
 
module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { 
        'primary-color':  '#52c41a',
        // 'text-color': '#fff',
        // 'text-color-secondary': '#fff',
        // 'heading-color': '#fff',
        // 'font-size-base': '18px',
        // 'item-active-bg': 'rgba(184, 209, 252,0.2)',
        // 'component-background': 'rgba(0,0,0,0)',
        // 'table-bg': '#fff',
        // 'menu-bg': 'rgba(0,0,0,0)',
        // 'layout-body-background': 'rgba(0,0,0,0)',
        // 'layout-header-background': 'rgba(0,0,0,0)',
        // 'popover-bg': 'rgba(155, 187, 242, 1)',
        // 'btn-primary-bg': '#389e0d',
        // 'switch-color': 'rgba(255, 0, 0, 0.7)',
        // 'table-header-bg': 'rgba(245,229,205,1)',
        // 'item-hover-bg': 'rgba(184, 209, 252,0.1)',
        // 'background-color-base':'rgba(0,0,0,0)',
        // 'select-dropdown-bg':'#597ef7'
    },
  }),
);