const LocalStorageService = (function() {
  let _service: any;
  function _getService(this: {
    getService: () => any;
    setToken: (tokenObj: any) => void;
    getAccessToken: () => string | null;
    getRefreshToken: () => string | null;
    clearToken: () => void;
  }) {
    if (!_service) {
      _service = this;
      return _service;
    }
    return _service;
  }
  function _setToken(tokenObj: any) {
    localStorage.setItem("access_token", JSON.stringify(tokenObj.access_token));
    //localStorage.setItem("refresh_token", JSON.stringify(tokenObj.refresh_token));
  }
  function _getAccessToken() {
    const access_token = localStorage.getItem("access_token");
    // if (access_token) {
    //   access_token = JSON.parse(access_token);
    //   return access_token;
    // }
    return access_token;
  }
  function _getRefreshToken() {
    const refresh_token = localStorage.getItem("refresh_token");
    // if (refresh_token) {
    //   refresh_token = JSON.parse(refresh_token);
    //   return refresh_token;
    // }
    return refresh_token;
  }
  function _clearToken() {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
  }
  return {
    getService: _getService,
    setToken: _setToken,
    getAccessToken: _getAccessToken,
    getRefreshToken: _getRefreshToken,
    clearToken: _clearToken
  };
})();
export default LocalStorageService;
