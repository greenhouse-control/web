//import config from 'config';
import axios from "axios";
import { AXIOS, baseURL } from "access/axios";
import LocalStorageService from "./local_storage.service";
import { notification } from "antd";
import i18n from "i18n";
export const userService = {
    login,
    logout,
    getAllUser,
};

async function login(username: any, password: any) {
    return axios
        .post(`${baseURL}/api/login`, {
            username: username,
            password: password,
        })
        .then((response) => {
            const access_token = response.data.access_token;
            const refresh_token = response.data.refresh_token;
            localStorage.setItem("access_token", JSON.stringify(access_token));
            localStorage.setItem(
                "refresh_token",
                JSON.stringify(refresh_token)
            );
            return username;
        })
        .catch((error) => {
            notification.error({
                message: i18n.t("Error!"),
                description: !error?.response?.data?.msg
                    ? i18n.t("Oops, something went wrong")
                    : i18n.t(error.response.data.msg),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
            logout();
            return Promise.reject(error);
        });
}

async function logout() {
    const localStorageService = LocalStorageService.getService();
    const access_token = JSON.parse(localStorageService.getAccessToken());
    const refresh_token = JSON.parse(localStorageService.getRefreshToken());

    // remove user from local storage to log user out
    if (access_token) {
        await axios
            .delete(`${baseURL}/api/logout/access-token`, {
                headers: {
                    Authorization: "Bearer " + access_token,
                },
            })
            .then()
            .catch((err) => {
                err.response && console.log("err", err.response);
            });
        localStorage.removeItem("access_token");
    }

    if (refresh_token) {
        await axios
            .delete(`${baseURL}/api/logout/refresh-token`, {
                headers: {
                    Authorization: "Bearer " + refresh_token,
                },
            })
            .then()
            .catch((err) => {
                err.response && console.log("err", err.response);
            });
        localStorage.removeItem("refresh_token");
    }
}

function getAllUser() {
    return AXIOS.get("api/user");
}
