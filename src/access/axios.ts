import axios from "axios";
import LocalStorageService from "redux/_services/local_storage.service";
import { history } from "redux/_helpers";
import { notification } from "antd";
import i18n from "i18n";

// LocalstorageService
const localStorageService = LocalStorageService.getService();
const baseURL = "http://192.168.0.4:5000";

let instance = axios.create({
    baseURL: baseURL,
});

// Add a request interceptor
instance.interceptors.request.use(
    (config) => {
        const token = JSON.parse(localStorageService.getAccessToken());
        if (token) {
            config.headers["Authorization"] = `Bearer ${token}`;
        }
        // config.headers['Content-Type'] = 'application/json';
        return config;
    },
    (error) => {
        Promise.reject(error);
    }
);

//Add a response interceptor
instance.interceptors.response.use(
    (response) => {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        console.log("resp:", response);
        return response;
    },
    async (error) => {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        if (
            error?.response?.status === 401 &&
            error?.response?.data?.msg === "The access token has expired"
        ) {
            const refreshToken = JSON.parse(
                localStorageService.getRefreshToken()
            );
            return axios
                .post(
                    `${baseURL}/api/refresh`,
                    {},
                    {
                        headers: {
                            Authorization: "Bearer " + refreshToken,
                        },
                    }
                )
                .then(async (res) => {
                    console.log("resp.interceptors.response", res);
                    localStorageService.setToken(res.data);
                    let instance_refesh = axios.create();
                    instance_refesh.defaults.headers.common = {};
                    instance_refesh.interceptors.request.use(
                        (config) => {
                            const token = JSON.parse(
                                localStorageService.getAccessToken()
                            );
                            if (token) {
                                config.headers[
                                    "Authorization"
                                ] = `Bearer ${res.data.access_token}`;
                            }
                            // config.headers['Content-Type'] = 'application/json';
                            return config;
                        },
                        (error) => {
                            Promise.reject(error);
                        }
                    );
                    const new_resp = await instance_refesh(error.config);
                    return new_resp;
                })
                .catch((error) => {
                    error.response.data &&
                        console.log("msg", error.response.data.msg);
                    if (
                        error?.response?.status === 401 &&
                        error?.response?.data?.msg ===
                            "The refresh token has expired"
                    ) {
                        history.push("/login");
                        notification.error({
                            message: i18n.t("Error!"),
                            description: !error?.response?.data?.msg
                                ? i18n.t("Oops, something went wrong")
                                : i18n.t(error.response.data.msg),
                            onClick: () => {
                                console.log("Oops, something went wrong");
                            },
                        });
                        //  return Promise.reject(error);
                    }
                });
        }
        return Promise.reject(error);
    }
);

const AXIOS = instance;
export { AXIOS, baseURL };
