import React, { Suspense } from 'react';
import { withTranslation } from 'react-i18next';

export default function ChangeLanguage (ComponentBase: any, props?: any) {
    const ComponentPrototype = withTranslation()(ComponentBase);
    return (
        <Suspense fallback="loading...">
            <ComponentPrototype {...props}/>
        </Suspense>
    );
}
