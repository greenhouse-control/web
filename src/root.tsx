import React from "react";
import { Provider } from "react-redux";
import AppWrapper from "./components/app_wrapper/page";
import { store } from "./redux/_helpers";
import { Router } from "react-router-dom";
import { history } from "redux/_helpers";
import "./root.css";

export default class RootApp extends React.PureComponent {
    render() {
        return (
            <Provider store={store}>
                <Router history={history}>
                    <AppWrapper />
                </Router>
            </Provider>
        );
    }
}
