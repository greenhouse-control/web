import React from "react";
import MenuBar from "../menu_bar";
import ContentPage from "../content_page";

import "./style.scss";
import "style/index.scss";
export default class AppWrapper extends React.PureComponent<{}, {}> {
    render() {
        return (
            <div className="app-wrapper">
                <MenuBar />
                <ContentPage />
            </div>
        );
    }
}
