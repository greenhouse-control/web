import React from 'react';
import { Menu } from 'antd';
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { WithTranslation } from 'react-i18next';

import ChangeLanguage from 'language';
import './style.scss';
import 'style/index.scss';

interface MenuBarProps extends WithTranslation, RouteComponentProps {
    t: any;
    location: any;
}
class MenuBarComponent extends React.PureComponent<MenuBarProps, {}> {
    render() {
        const { t } = this.props;
        return (
            <Menu
                theme="light"
                mode="horizontal"
                className="menu"
                selectedKeys={[this.props.location.pathname.split('/')[1]]}
            >
                <Menu.Item key="info"><Link to='/info'><span>{t('Info')}</span></Link></Menu.Item>
                <Menu.Item key="tools"><Link to='/tools'><span>{t('Tools')}</span></Link></Menu.Item>
                <Menu.Item key="home" ><Link to='/home'><span>{t('Home')}</span></Link></Menu.Item>
            </Menu>
        )
    }
}

const MenuBar = React.memo(() => ChangeLanguage(withRouter(MenuBarComponent)));
export default MenuBar;