import React from 'react';
import { Route, Switch } from "react-router-dom";
import Home from 'components/home/page';
import Tools from 'components/tools/page';
import Info from 'components/info/page';
import { PrivateRoute } from 'redux/_components';
import { WelcomePage } from 'components/welcome_page';
import { LoginPage } from 'components/login_page';


export default class ContentPage extends React.PureComponent {

    render() {
        return (
            <Switch>
                <Route path="/login" component={LoginPage} />
                <PrivateRoute exact path="/" component={WelcomePage} />
                <PrivateRoute path="/home" component={Home} />
                <PrivateRoute path="/tools" component={Tools} />
                <PrivateRoute path="/info" component={Info} />
            </Switch>
        )
    }

}

