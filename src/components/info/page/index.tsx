import React from "react";
import { WithTranslation } from "react-i18next";
import ChangeLanguage from "language";

import { AXIOS } from "access/axios";
import { Spin, Icon, notification } from "antd";

import "./style.scss";
import "style/index.scss";
import { Link } from "react-router-dom";

interface InfoProps extends WithTranslation {
    t: any;
}
type InfoState = {
    data: any;
    loading: boolean;
};

const user_info = localStorage.getItem("user_info");

class InfoView extends React.PureComponent<InfoProps, InfoState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: user_info ? JSON.parse(user_info) : {},
            loading: false,
        };
    }
    componentDidMount() {
        if (!user_info) {
            this.fetch_infor();
        }
    }
    fetch_infor = async () => {
        const API_INFO = "/api/user/current";
        try {
            this.setState({ loading: true });
            const resp = await AXIOS.get(API_INFO);
            const data = resp.data;
            this.setState({ data: data });
        } catch (error) {
            console.log("err", error);
            notification.error({
                message: this.props.t("Error!"),
                description: !(error?.response?.data?.msg)
                    ? this.props.t("Oops, something went wrong")
                    : this.props.t(error.response.data.msg),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
        } finally {
            this.setState({ loading: false });
        }
    };

    render() {
        const { t } = this.props;
        console.log("render:: info");
        if (this.state.loading) return <Spin />;
        return (
            <div className="layout">
                <div className="content-info">
                    <h2>{t("Info")}</h2>
                    <div className="info-card">
                        <div className="info-card-item">
                            <div className="info-card-item-param">
                                {t("Username")}
                            </div>
                            <div className="info-card-item-value">
                                {this.state.data.username}
                            </div>
                        </div>
                        <div className="info-card-item">
                            <div className="info-card-item-param">
                                {t("Name")}
                            </div>
                            <div className="info-card-item-value">
                                {this.state.data.first_name}{" "}
                                {this.state.data.last_name}
                            </div>
                        </div>
                        <div className="info-card-item">
                            <div className="info-card-item-param">
                                {t("Admin")}
                            </div>
                            <div className="info-card-item-value">
                                {this.state.data.admin ? t('Yes') : t('No')}
                            </div>
                        </div>
                        <div className="info-card-item">
                            <div className="info-card-item-param">
                                {t("Email")}
                            </div>
                            <div className="info-card-item-value">
                                {this.state.data.email}
                            </div>
                        </div>
                    </div>
                    <div className="logout-button">
                        <Link to="/login">
                            <Icon type="logout" /> {t("Logout")}
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}
const Info = React.memo(() => ChangeLanguage(InfoView));
export default Info;
