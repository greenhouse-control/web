import React from "react";
import { Link } from "react-router-dom";
import { Button, notification } from "antd";
import "./style.scss";
import ChangeLanguage from "language";
import { WithTranslation } from "react-i18next";
import { AXIOS } from "access/axios";

interface WelcomePageProps extends WithTranslation {
    t: any;
}
type WelcomePageState = {
    loading: boolean;
};
class WelcomePageView extends React.PureComponent<WelcomePageProps, WelcomePageState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
        };
    }
    componentDidMount() {
        this.fetch_infor();
    }
    fetch_infor = async () => {
        const API_INFO = "/api/user/current";
        try {
            this.setState({ loading: true });
            const resp = await AXIOS.get(API_INFO);
            const data = resp.data;
            localStorage.setItem("user_info",JSON.stringify(data));
        } catch (error) {
            console.log("err", error);
            notification.error({
                message: this.props.t("Error!"),
                description: !(error?.response?.data?.msg) ? this.props.t("Oops, something went wrong") : this.props.t(error.response.data.msg),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
        } finally {
            this.setState({ loading: false });
        }
    };

    render() {
        return (
            <div className="welcome-page">
                <br />
                <h2 className="login-head-text">
                    {this.props.t("Logged in successfully")}
                </h2>
                <p>
                    <Button type="primary" className="button-start-now">
                        <Link to="/home">
                            <span className="welcom-button-text">
                                {this.props.t("Start now")}
                            </span>
                        </Link>
                    </Button>
                </p>
            </div>
        );
    }
}

export const WelcomePage = React.memo(() => ChangeLanguage(WelcomePageView));
