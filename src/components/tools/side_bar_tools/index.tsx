import React from "react";
import { Layout, Menu, Icon } from "antd";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";
import ChangeLanguage from "language";
import { WithTranslation } from "react-i18next";

import "./style.scss";
import "style/index.scss";

const { Sider } = Layout;
const { SubMenu } = Menu;
const isAdmin = () => JSON.parse(localStorage.getItem("user_info") || "").admin;
console.log('isAdmin',isAdmin);
interface SideBarToolsProps extends WithTranslation, RouteComponentProps {
    t: any;
    location: any;
}
type SideBarToolsState = {
    collapsed: boolean;
};
class SideBarToolsComponent extends React.PureComponent<
    SideBarToolsProps,
    SideBarToolsState
> {
    constructor(props: any) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        const { t, location } = this.props;
        return (
            <div>
                <Sider
                    theme="light"
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}
                >
                    <Menu
                        mode="inline"
                        theme="light"
                        selectedKeys={[location.pathname]}
                        defaultOpenKeys={["/tools"]}
                    >
                        <Menu.Item>
                            <Icon
                                className="trigger"
                                type={
                                    this.state.collapsed
                                        ? "menu-unfold"
                                        : "menu-fold"
                                }
                                onClick={this.toggle}
                            />
                        </Menu.Item>

                        <SubMenu
                            key="/tools"
                            title={
                                <span>
                                    <Icon
                                        type="bar-chart"
                                        className="statistic"
                                    />
                                    <span className="statistic">
                                        {t("Statistic")}
                                    </span>
                                </span>
                            }
                        >
                            <Menu.Item key="/tools/table">
                                <Link to="/tools/table">
                                    <Icon type="table" className="statistic" />
                                    <span className="statistic">
                                        {t("Table")}
                                    </span>
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="/tools/chart">
                                <Link to="/tools/chart">
                                    <Icon
                                        type="line-chart"
                                        className="statistic"
                                    />
                                    <span className="statistic">
                                        {t("Chart")}
                                    </span>
                                </Link>
                            </Menu.Item>
                        </SubMenu>

                        {/* <Menu.Item key="/tools/config">
                            <Link to="/tools/config">
                                <Icon type="setting" className="config" />
                                <span className="config">{t("Config")}</span>
                            </Link>
                        </Menu.Item> */}

                        {isAdmin && (
                            <Menu.Item key="/tools/account-mgt">
                                <Link to="/tools/account-mgt">
                                    <Icon type="user" className="account-mgt" />
                                    <span className="account-mgt">
                                        {t("Account-mgt")}
                                    </span>
                                </Link>
                            </Menu.Item>
                        )}
                    </Menu>
                </Sider>
            </div>
        );
    }
}

const SideBarTools = React.memo(() =>
    ChangeLanguage(withRouter(SideBarToolsComponent))
);
export default SideBarTools;
