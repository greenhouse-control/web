import React from "react";
import SideBarTools from "../side_bar_tools";
import ContentTools from "../content_tools";
import { Redirect } from "react-router-dom";
import "./style.scss";

export default class Tools extends React.PureComponent<{}, {}> {
    render() {
        return (
            <div className="layout">
                <Redirect to={{ pathname: "/tools/table" }} />
                <SideBarTools />
                <ContentTools />
            </div>
        );
    }
}
