import React from "react";
import { Table, notification, Select } from "antd";
import { ColumnProps } from "antd/es/table";
import { AXIOS } from "access/axios";
import ChangeLanguage from "language";
import { WithTranslation } from "react-i18next";
import { DateRange } from "../chart";
import moment from "moment";

import "./style.scss";
import "style/index.scss";

interface TableStatisticProps extends WithTranslation {
    t: any;
}
type TableStatisticState = {
    data: any;
    time: any;
    startDate: any;
    endDate: any;
    loading: boolean;
    cluster: number;
};
interface DataTable {
    key: string;
    index: string;
    employee_name: string;
    checkin: string;
    checkout: string;
    approvedby: string;
    status: string;
}

class TableStatisticComponent extends React.PureComponent<
    TableStatisticProps,
    TableStatisticState
> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            startDate: moment().subtract(1, "days").format("YYYY-MM-DD"),
            endDate: moment().format("YYYY-MM-DD"),
            loading: false,
            cluster: 0,
            time: [],
        };
    }

    onChangeShowTable = async (start: any, end: any) => {
        try {
            this.setState({
                startDate: start,
                endDate: end,
                time: [],
                data: [],
                loading: true,
            });
            console.log("Select-date", start, end);
            const start_early_7h = moment(start)
                .subtract(7, "hours")
                .format("YYYY-MM-DDTHH:mm:ss");
            const end_early_7h = moment(end)
                .subtract(7, "hours")
                .format("YYYY-MM-DDTHH:mm:ss");
            const API_GET_DATA = `api/sensors-statistic-${this.state.cluster}`;
            const APT_FILTER = `/filter:"time"from"'${start_early_7h}'"to"'${end_early_7h}'"`;
            const API = API_GET_DATA + APT_FILTER;
            const resp = await AXIOS.get(API);
            const prev_data = resp.data;
            console.log('data',typeof prev_data);
            const data = prev_data.map((item: any, index: number) => {
                return {
                    key: index,
                    time: moment(`${item.time}`).format("YYYY-MM-DD HH:mm:ss"),
                    temperature: item.temperature ?? 0,
                    humidity: item.humidity ?? 0,
                    light: item.light ?? 0,
                    concentration_of_co2: item.concentration_of_co2,
                    water_flow: item.water_flow,
                };
            });
            this.setState({ data: data });
            notification.success({
                message: this.props.t("Successfully!"),
                description: this.props.t("Get data of sensors successfully"),
                onClick: () => {
                    console.log("Get data of sensors successfully");
                },
            });
            data.length === 0 &&
                notification.warning({
                    message: this.props.t("Warning!"),
                    description: this.props.t("No data from sensors"),
                    onClick: () => {
                        console.log("No data from sensors");
                    },
                });
        } catch (err) {
            console.log("err", err);
            notification.error({
                message: this.props.t("Error!"),
                description: this.props.t("Oops, something went wrong"),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
        } finally {
            this.setState({ loading: false });
        }
    };

    onChangeCluster = (value: number) => {
        this.setState({ cluster: value });
    };

    columns: ColumnProps<DataTable>[] = [
        {
            title: "Id",
            dataIndex: "key",
            key: "key",
            align: "center",
            fixed: "left",

            sorter: (a: any, b: any) => a.key - b.key,
            sortDirections: ["descend", "ascend"],
        },
        {
            title: this.props.t("Time"),
            dataIndex: "time",
            key: "time",
            align: "center",
            width: 200,
            fixed: "left",

            sorter: (a: any, b: any) =>
                new Date(a.time).getTime() - new Date(b.time).getTime(),
            sortDirections: ["descend", "ascend"],
        },
        {
            title: this.props.t("Temp"),
            dataIndex: "temperature",
            key: "temperature",
            align: "center",

            sorter: (a: any, b: any) => a.temperature - b.temperature,
            sortDirections: ["descend", "ascend"],
        },
        {
            title: this.props.t("Humidity"),
            dataIndex: "humidity",
            key: "humidity",
            align: "center",

            sorter: (a: any, b: any) => a.humidity - b.humidity,
            sortDirections: ["descend", "ascend"],
        },
        {
            title: this.props.t("Light"),
            dataIndex: "light",
            key: "light",
            align: "center",

            sorter: (a: any, b: any) => a.light - b.light,
            sortDirections: ["descend", "ascend"],
        },
        {
            title: "CO2",
            dataIndex: "concentration_of_co2",
            key: "concentration_of_co2",
            align: "center",

            sorter: (a: any, b: any) =>
                a.concentration_of_co2 - b.concentration_of_co2,
            sortDirections: ["descend", "ascend"],
        },

        {
            title: this.props.t("Water flow"),
            dataIndex: "water_flow",
            key: "water_flow",
            align: "center",

            sorter: (a: any, b: any) => a.water_flow - b.water_flow,
            sortDirections: ["descend", "ascend"],
        },
    ];

    render() {
        const { t } = this.props;
        const { Option } = Select;
        console.log("render:: table statistic");
        return (
            <div className="table-statistic-wrap">
                <div className="select-item">
                    <span>{t("Select sensor cluster")}: </span>
                    <Select
                        placeholder={t("Select sensor cluster")}
                        style={{ width: "250px" }}
                        onChange={this.onChangeCluster}
                        defaultValue={0}
                    >
                        <Option value={0}>{`${t("Sensor cluster")} 0`}</Option>
                        <Option value={1}>{`${t("Sensor cluster")} 1`}</Option>
                    </Select>
                </div>
                <div className="select-time">
                    <span>{t("Select date")}:</span>
                    <DateRange onChange={this.onChangeShowTable} />
                </div>
                <div>
                    <Table
                        columns={this.columns}
                        dataSource={this.state.data}
                        size="small"
                        className="table-statistic"
                        loading={this.state.loading}
                        scroll={{ x: 1000 }}
                        pagination={{ pageSize: 20 }}
                    />
                </div>
            </div>
        );
    }
}

const TableStatistic = React.memo(() =>
    ChangeLanguage(TableStatisticComponent)
);
export default TableStatistic;
