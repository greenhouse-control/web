import React from "react";
import { Button, Select, Spin, notification } from "antd";
import { AXIOS } from "access/axios";
import { DatePicker } from "antd";
import ChangeLanguage from "language";
import { Line } from "react-chartjs-2";
import moment from "moment";
import { WithTranslation } from "react-i18next";

import "./style.scss";
import "style/index.scss";

interface DateRangeProps extends WithTranslation {
    onChange: any;
    t: any;
}
type DateRangeState = {
    startValue: any;
    endValue: any;
    startDate: any;
    endDate: any;
    endOpen: boolean;
};
class DateRangeView extends React.PureComponent<
    DateRangeProps,
    DateRangeState
> {
    state = {
        startValue: moment().subtract(1, "days"),
        endValue: moment(),
        startDate: moment().subtract(1, "days").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        endOpen: false,
    };
    // shouldComponentUpdate(nextProps: any, nextState: any) {
    //     const {
    //         startValue,
    //         endValue,
    //         startDate,
    //         endDate,
    //         endOpen,
    //     } = this.state;
    //     const {
    //         startValueNext,
    //         endValueNext,
    //         startDateNext,
    //         endDateNext,
    //         endOpenNext,
    //     } = nextState;
    //     return (
    //         startValue !== startValueNext ||
    //         endValue !== endValueNext ||
    //         startDate !== startDateNext ||
    //         endDate !== endDateNext ||
    //         endOpen !== endOpenNext
    //     );
    // }
    disabledStartDate = (startValue: any) => {
        const { endValue } = this.state;
        if (!startValue || !endValue) {
            return false;
        }
        return startValue.valueOf() > endValue.valueOf();
    };

    disabledEndDate = (endValue: any) => {
        const { startValue } = this.state;
        if (!endValue || !startValue) {
            return false;
        }
        return endValue.valueOf() <= startValue.valueOf();
    };

    onStartChange = (value: any, dateString: string) => {
        this.setState({
            startValue: value,
            startDate: dateString,
        });
        console.log("start", dateString);
    };

    onEndChange = (value: any, dateString: string) => {
        this.setState({
            endValue: value,
            endDate: dateString,
        });
        console.log("end", dateString);
    };

    onChange = () => {
        this.props.onChange &&
            this.props.onChange(this.state.startDate, this.state.endDate);
        console.log(
            "Pendding-change",
            this.state.startDate,
            this.state.endDate
        );
    };

    handleStartOpenChange = (open: any) => {
        if (!open) {
            this.setState({ endOpen: true });
        }
    };

    handleEndOpenChange = (open: any) => {
        this.setState({ endOpen: open });
    };

    render() {
        console.log("render:: DateRange Picker");
        const dateFormat = "YYYY-MM-DD";
        const { t } = this.props;
        return (
            <div className="date-range">
                <div className="date-range-item">
                    <DatePicker
                        disabledDate={this.disabledStartDate}
                        showTime={false}
                        format={dateFormat}
                        value={this.state.startValue}
                        placeholder={t("Start")}
                        onChange={this.onStartChange}
                        onOpenChange={this.handleStartOpenChange}
                    />
                </div>
                <div className="date-range-item">
                    <DatePicker
                        disabledDate={this.disabledEndDate}
                        showTime={false}
                        format={dateFormat}
                        value={this.state.endValue}
                        placeholder={t("End")}
                        onChange={this.onEndChange}
                        open={this.state.endOpen}
                        onOpenChange={this.handleEndOpenChange}
                    />
                </div>
                <div className="date-range-item">
                    <Button
                        type="primary"
                        icon="export"
                        onClick={this.onChange}
                    >
                        {t("Show")}
                    </Button>
                </div>
            </div>
        );
    }
}

const DateRange = React.memo((props: any) =>
    ChangeLanguage(DateRangeView, props)
);
interface ChartStatisticProps extends WithTranslation {
    t: any;
}
type ChartStatisticState = {
    startDate: any;
    endDate: any;
    time: any;
    para: any;
    name_para: string;
    loading: boolean;
    cluster: number;
};
class ChartStatisticComponent extends React.PureComponent<
    ChartStatisticProps,
    ChartStatisticState
> {
    constructor(props: any) {
        super(props);
        this.state = {
            startDate: moment().subtract(1, "days").format("YYYY-MM-DD"),
            endDate: moment().format("YYYY-MM-DD"),
            time: [],
            para: [],
            name_para: "item.temperature",
            loading: false,
            cluster: 0,
        };
    }

    onChangeChart = async (start: any, end: any) => {
        try {
            this.setState({
                startDate: start,
                endDate: end,
                time: [],
                para: [],
                loading: true,
            });
            const start_early_7h = moment(start)
                .subtract(7, "hours")
                .format("YYYY-MM-DDTHH:mm:ss");
            const end_early_7h = moment(end)
                .subtract(7, "hours")
                .format("YYYY-MM-DDTHH:mm:ss");
            const API_GET_DATA = `api/sensors-statistic-${this.state.cluster}`;
            const APT_FILTER = `/filter:"time"from"'${start_early_7h}'"to"'${end_early_7h}'"`;
            const API = API_GET_DATA + APT_FILTER;
            const resp = await AXIOS.get(API);
            const data = resp.data;
            const time = [] as any;
            const param = data.map((item: any, index: number) => {
                time.push(moment(`${item.time}`).format("YYYY-MM-DD HH:mm:ss"));
                return eval(this.state.name_para);
            });
            this.setState({
                time: time,
                para: param,
            });
            notification.success({
                message: this.props.t("Successfully!"),
                description: this.props.t("Get data of sensors successfully"),
                onClick: () => {
                    console.log("Get data of sensors successfully");
                },
            });
            data.length ===  0 && notification.warning({
                message: this.props.t("Warning!"),
                description: this.props.t(
                    "No data from sensors"
                ),
                onClick: () => {
                    console.log("No data from sensors");
                },
            });
        } catch (err) {
            console.log("err", err);
            notification.error({
                message: this.props.t("Error!"),
                description: this.props.t("Oops, something went wrong"),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
        } finally {
            this.setState({ loading: false });
        }
    };

    onChangePara = (value: string) => {
        this.setState({
            name_para: value,
            para: [],
            time: [],
        });
    };
    onChangeCluster = (value: number) => {
        this.setState({ cluster: value });
    };

    render() {
        console.log("render:: chart statistic");
        const { t } = this.props;
        const { Option } = Select;
        const dataChart = {
            labels: this.state.time,
            //labels: [time[0], time[num*1], time[num*2], time[num*3], time[num*4], time[num*5], time[num*6], time[num*7], time[num*8], time[num*9], time[lengthChart-1]],
            datasets: [
                {
                    label: `${t("Statistic of")} ${t(
                        this.state.name_para.substr(5)
                    )} `,
                    fill: false,
                    lineTension: 0.1,
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: "butt",
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: "miter",
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.state.para,
                },
            ],
        };

        return (
            <div className="statistic-chart">
                <div className="select-time">
                    <div className="select-item">
                        <span>{t("Select parameter")}: </span>
                        <Select
                            placeholder={t("Select parameter")}
                            style={{ width: "250px" }}
                            onChange={this.onChangePara}
                            defaultValue="item.temperature"
                        >
                            <Option value="item.temperature">
                                {t("Temperature")}
                            </Option>
                            <Option value="item.humidity">
                                {t("Humidity")}
                            </Option>
                            <Option value="item.light">{t("Light")}</Option>
                            <Option value="item.concentration_of_co2">
                                {t("Concentration of CO")}
                                <sub>2</sub>
                            </Option>
                            <Option value="item.water_flow">
                                {t("Water flow")}
                            </Option>
                        </Select>
                    </div>
                    <div className="select-item">
                        <span>{t("Select sensor cluster")}: </span>
                        <Select
                            placeholder={t("Select sensor cluster")}
                            style={{ width: "250px" }}
                            onChange={this.onChangeCluster}
                            defaultValue={0}
                        >
                            <Option value={0}>
                                {`${t("Sensor cluster")} 0`}
                            </Option>
                            <Option value={1}>
                                {`${t("Sensor cluster")} 1`}
                            </Option>
                        </Select>
                    </div>
                    <div className="select-item">
                        <div>{t("Select date")}:</div>
                        <DateRange onChange={this.onChangeChart} />
                    </div>
                </div>

                <div>
                    {this.state.loading ? (
                        <Spin />
                    ) : (
                        <Line
                            ref="chart"
                            data={dataChart}
                            width={1000}
                            height={500}
                        />
                    )}
                </div>
            </div>
        );
    }
}

const ChartStatistic = React.memo(() =>
    ChangeLanguage(ChartStatisticComponent)
);
export default ChartStatistic;

export { DateRange };
