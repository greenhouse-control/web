import React from 'react';
import { Popconfirm, Icon, Table, Divider, notification } from 'antd';
import { ColumnProps } from 'antd/es/table';
import FormCreate from './form_create';
import FormUpdate from './form_update';
import { AXIOS } from 'access/axios';
import { WithTranslation } from "react-i18next";
import ChangeLanguage from 'language';

import './style.scss';
import 'style/index.scss';

interface AccountMgtProps extends WithTranslation {
    t: any;
}
interface User {
    key: string,
    id: string,
    index: string,
    uuid: string,
    username: string,
    email: string,
    admin: string,

}

class AccountMgtView extends React.PureComponent<AccountMgtProps, { data: any, isAdmin: boolean, isLoading: boolean, }> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            isAdmin: false,
            isLoading: false,
        };

    }
    fetcher_all_user = async () => {
        let API_GET_ALL_USERS = 'api/user';

        try {
            this.setState({ isLoading: true });
            const resp = await (AXIOS.get(API_GET_ALL_USERS));
            const items = resp.data
            const data = items.map((item: any, index: number) => {
                return {
                    key: `${item.public_id}`,
                    id: item.id,
                    index: item.id,
                    uuid: item.public_id,
                    username: item.username,
                    email: item.email,
                    admin: item.admin ? this.props.t('Yes') : this.props.t('No'),
                    first_name: item.first_name,
                    last_name: item.last_name,
                };
            });
            this.setState({ data: data });
        }
        catch (error) {
            notification.error({
                message: this.props.t("Error!"),
                description: !(error?.response?.data?.msg) ? this.props.t("Oops, something went wrong") : this.props.t(error.response.data.msg),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
        }
        finally {
            this.setState({ isLoading: false });
        }
    };
    componentDidMount() {
        this.fetcher_all_user();
    }

    handleCreateAccount = () => {
        this.fetcher_all_user();
    }
    handleUpdateAccount = () => {
        this.fetcher_all_user();
    }

    deleteAccount = (uuid: any) => {
        console.log(uuid, 'deleted');
        const API_DELETE_ACCOUNT = `api/user/${uuid}`;
        AXIOS.delete(API_DELETE_ACCOUNT)
            .then(() => {
                this.fetcher_all_user();
                notification.success({
                    message: this.props.t("Successfully!"),
                    description: this.props.t("Delete account successfully"),
                    onClick: () => {
                        console.log("Delete account successfully");
                    },
                });
            })
            .catch((err) => {
                notification.error({
                    message: this.props.t("Error!"),
                    description: !(err?.response?.data?.msg) ? this.props.t("Oops, something went wrong") : this.props.t(err.response.data.msg),
                    onClick: () => {
                        console.log("Oops, something went wrong");
                    },
                });
            });
    }

    columns: ColumnProps<User>[] = [
        {
            title: "Id",
            dataIndex: 'uuid',
            render: (value) => (<span>{value.slice(0,5)}...</span>),
            key: 'uuid',
            align: 'center',
            width: 70,

        },
        {
            title: this.props.t('Username'),
            dataIndex: 'username',
            key: 'username',
            align: 'center',
            sorter: (a: any, b: any) => a.username.length - b.username.length,
            width: 200,

        },
        {
            title: this.props.t('Email'),
            dataIndex: 'email',
            key: 'email',
            align: 'center',
            sorter: (a: any, b: any) => a.email.localeCompare(b.email),
            width: 200,
        },
        {
            title: this.props.t('Admin'),
            dataIndex: 'admin',
            key: 'admin',
            align: 'center',
            width: 70,
        },
        {
            title: this.props.t('Name'),
            children: [
                {
                    title: this.props.t('First name'),
                    dataIndex: 'first_name',
                    key: 'first_name',
                    align: 'center',
                    width: 200,
                },
                {
                    title: this.props.t('Last name'),
                    dataIndex: 'last_name',
                    key: 'last_name',
                    align: 'center',
                    width: 200,
                },
            ],
        },

        {
            title: this.props.t('Action'),
            dataIndex: 'action',
            key: 'action',
            align: 'center',
            render: (text: any, record: any) => (
                <span className="action">
                    <FormUpdate uuid={record.uuid} handleUpdateAccount={this.handleUpdateAccount} />
                    <Divider type="vertical" />
                    <Popconfirm
                        title={this.props.t("Are you sure delete this account?")}
                        onConfirm={() => this.deleteAccount(record.uuid)}
                        onCancel={() => ''}
                        okText={this.props.t("Yes")}
                        cancelText={this.props.t("No")}
                    >
                        <div className="icon-action">
                            <Icon
                                type="delete"
                                style={{
                                    color: 'black'
                                }}
                            /></div>
                    </Popconfirm>
                </span>
            ),
            width: 100,
        },
    ];
    render() {
        console.log('render:: acount-mng');
        return (
            <div className="acount-mng">
                <div className="form-create" ><FormCreate handleCreateAccount={this.handleCreateAccount} /> </div>
                <Table 
                    columns={this.columns} 
                    dataSource={this.state.data} 
                    bordered 
                    size="small"
                    className="table-account"
                    scroll={{x: 1000}} 
                    pagination={{pageSize : 20}}
                    loading={this.state.isLoading}
                    />
            </div>
        )
    }
}

const AccountMgt = React.memo(()=> ChangeLanguage(AccountMgtView));
export default AccountMgt;