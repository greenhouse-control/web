import React from "react";
import { Modal, Icon, Form, Input, Button, notification } from "antd";
import { FormComponentProps } from "antd/es/form";
import { AXIOS } from "access/axios";
import { WithTranslation } from "react-i18next";
import ChangeLanguage from "language";

import "./style.scss";
import "style/index.scss";

interface UserFormProps extends FormComponentProps, WithTranslation {
    form: any;
    handleUpdateAccount: Function;
    uuid: string;
    t: any;
}
type UpdateFormState = {
    visible: boolean;
    confirmDirty: boolean;
    loading: boolean;
    password: string;
    uuid: string;
    first_name: string;
    last_name: string;
    changePassword: boolean;
};
class UpdateForm extends React.PureComponent<UserFormProps, UpdateFormState> {
    state = {
        visible: false,
        confirmDirty: false,
        loading: false,
        password: "",
        uuid: this.props.uuid,
        first_name: "",
        last_name: "",
        changePassword: false,
    };
    handleChangePassword = () => {
        this.setState({ changePassword: !this.state.changePassword });
    };
    updateAccount = (e: any) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err: any, values: any) => {
            if (err) {
                return console.log("err", err);
            }
            console.log("values", values);
            this.setState({ loading: true });
            const API_UPDATE_ACCOUNT = `api/user/${this.state.uuid}`;
            AXIOS.put(API_UPDATE_ACCOUNT, {
                first_name: values.first_name,
                last_name: values.last_name,
                ...(values.password !== "" && { password: values.password }),
            })
                .then((response) => {
                    this.props.handleUpdateAccount &&
                        this.props.handleUpdateAccount();
                    this.setState({ visible: false });
                    notification.success({
                        message: this.props.t("Successfully!"),
                        description: this.props.t(
                            "Update account successfully"
                        ),
                        onClick: () => {
                            console.log("Update account successfully.");
                        },
                    });
                })
                .catch((error) => {
                    notification.error({
                        message: this.props.t("Error!"),
                        description: !error?.response?.data?.msg
                            ? this.props.t("Oops, something went wrong")
                            : this.props.t(error.response.data.msg),
                        onClick: () => {
                            console.log("Oops, something went wrong");
                        },
                    });
                });

            this.setState({ loading: false });
        });
    };

    handleConfirmBlur = (e: any) => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    };

    compareToFirstPassword = (rule: any, value: any, callback: any) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue("password")) {
            callback(
                this.props.t("Two passwords that you enter is inconsistent!")
            );
        } else {
            callback();
        }
    };

    validateToNextPassword = (rule: any, value: any, callback: any) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
            form.validateFields(["confirm"], { force: true });
        }
        callback();
    };

    showModal = () => {
        this.props.form.resetFields();
        this.setState({
            visible: true,
            changePassword: false,
        });
        const API_UPDATE_ACCOUNT = `api/user/${this.state.uuid}`;
        AXIOS.get(API_UPDATE_ACCOUNT)
            .then((resp) => {
                this.setState({
                    first_name: resp.data.first_name,
                    last_name: resp.data.last_name,
                });
            })
            .catch((error) => {
                notification.error({
                    message: this.props.t("Error!"),
                    description: !error?.response?.data?.msg
                        ? this.props.t("Oops, something went wrong")
                        : this.props.t(error.response.data.msg),
                    onClick: () => {
                        console.log("Oops, something went wrong");
                    },
                });
                //return Promise.reject(error.response.data.msg);
            });
    };

    handleCancel = (e: any) => {
        this.setState({
            visible: false,
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const { t } = this.props;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };

        return (
            <div className="form-update">
                <div
                    className="icon-action"
                    style={{ color: "blue" }}
                    onClick={this.showModal}
                >
                    {<Icon type="edit" />}
                </div>

                <Modal
                    title={t("Update account form")}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    footer={null}
                >
                    <Form {...formItemLayout} onSubmit={this.updateAccount}>
                        <Form.Item label={t("First name")}>
                            {getFieldDecorator("first_name", {
                                rules: [
                                    {
                                        required: true,
                                        message: t(
                                            "Please input your first name!"
                                        ),
                                    },
                                ],
                                initialValue: this.state.first_name,
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label={t("Last name")}>
                            {getFieldDecorator("last_name", {
                                rules: [
                                    {
                                        required: true,
                                        message: t(
                                            "Please input your last name!"
                                        ),
                                    },
                                ],
                                initialValue: this.state.last_name,
                            })(<Input />)}
                        </Form.Item>
                        <div
                            className="change-password-action"
                            onClick={this.handleChangePassword}
                        >
                            {t("Change password")}
                        </div>
                        {this.state.changePassword && (
                            <div>
                                <Form.Item label={t("Password")} hasFeedback>
                                    {getFieldDecorator("password", {
                                        rules: [
                                            {
                                                required: true,
                                                message: t(
                                                    "Please input your password!"
                                                ),
                                            },
                                            {
                                                validator: this
                                                    .validateToNextPassword,
                                            },
                                        ],
                                    })(<Input.Password />)}
                                </Form.Item>
                                <Form.Item
                                    label={t("Confirm password")}
                                    hasFeedback
                                >
                                    {getFieldDecorator("confirm", {
                                        rules: [
                                            {
                                                required: true,
                                                message: t(
                                                    "Please confirm your password!"
                                                ),
                                            },
                                            {
                                                validator: this
                                                    .compareToFirstPassword,
                                            },
                                        ],
                                    })(
                                        <Input.Password
                                            onBlur={this.handleConfirmBlur}
                                        />
                                    )}
                                </Form.Item>
                            </div>
                        )}

                        <div className="button-group">
                            <div className="button-action">
                                <Button key="back" onClick={this.handleCancel}>
                                    {t("Cancel")}
                                </Button>
                            </div>
                            <div className="button-action">
                                <Button
                                    key="submit"
                                    type="primary"
                                    htmlType="submit"
                                    loading={this.state.loading}
                                    onClick={this.updateAccount}
                                >
                                    {t("Apply")}
                                </Button>
                            </div>
                        </div>
                    </Form>
                </Modal>
            </div>
        );
    }
}

const BFormUpdate = Form.create<UserFormProps>({ name: "register" })(
    UpdateForm
);
const FormUpdate = React.memo((props: any) =>
    ChangeLanguage(BFormUpdate, props)
);
export default FormUpdate;
