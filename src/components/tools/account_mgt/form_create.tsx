import React from "react";
import { Modal, Checkbox, Form, Input, Button, notification } from "antd";
import { FormComponentProps } from "antd/es/form";
import { AXIOS } from "access/axios";
import ChangeLanguage from "language";
import { WithTranslation } from "react-i18next";

import "./style.scss";
import "style/index.scss";

interface UserFormProps extends FormComponentProps, WithTranslation {
    form: any;
    handleCreateAccount: Function;
    t: any
}
type UserFormState = {
    visible: boolean,
    confirmDirty: boolean,
    loading: boolean,
    username: string
    password: string,
    email:string,
}
class RegistrationForm extends React.PureComponent<UserFormProps, UserFormState> {
    state = {
        visible: false,
        confirmDirty: false,
        loading: false,
        username: "",
        password: "",
        email: "",
    };
    handleSubmit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err: any, values: any) => {
            if (err) {
                return console.log("err", err);
            }
            console.log("values", values);
            this.setState({ loading: true });
            AXIOS.post("api/user", {
                username: values.username,
                password: values.password,
                email: values.email,
                admin: Boolean(values.admin),
                first_name: values.first_name,
                last_name: values.last_name,
            })
                .then((response) => {
                    this.setState({ visible: false });
                    this.props.handleCreateAccount &&
                        this.props.handleCreateAccount();
                        notification.success({
                            message: this.props.t("Successfully!"),
                            description: this.props.t("Create account successfully"),
                            onClick: () => {
                                console.log("Create account successfully");
                            },
                        });
                })
                .catch((error) => {
                    notification.error({
                        message: this.props.t("Error!"),
                        description: !(error?.response?.data?.msg) ? this.props.t("Oops, something went wrong") : this.props.t(error.response.data.msg),
                        onClick: () => {
                            console.log("Oops, something went wrong");
                        },
                    });
                    //return Promise.reject(error.response.data.msg);
                    this.setState({ visible: true });
                });

            this.setState({ loading: false });
        });
    };

    handleConfirmBlur = (e: any) => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    };

    compareToFirstPassword = (rule: any, value: any, callback: any) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue("password")) {
            callback(this.props.t("Two passwords that you enter is inconsistent!"));
        } else {
            callback();
        }
    };

    validateToNextPassword = (rule: any, value: any, callback: any) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
            form.validateFields(["confirm"], { force: true });
        }
        callback();
    };

    showModal = () => {
        this.props.form.resetFields();
        this.setState({
            visible: true,
        });
    };

    handleCancel = (e: any) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const { t } = this.props;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (
            <div>
                <Button type="primary" onClick={this.showModal}>
                    {t('Create account')}
                </Button>
                <Modal
                    title={t("Create account form")}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    footer={null}
                >
                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                        <Form.Item label={t("Username")}>
                            {getFieldDecorator("username", {
                                rules: [
                                    {
                                        required: true,
                                        message: t("Please input your username!"),
                                    },
                                ],
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label={t("First name")}>
                            {getFieldDecorator("first_name", {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            t("Please input your first name!"),
                                    },
                                ],
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label={t("Last name")}>
                            {getFieldDecorator("last_name", {
                                rules: [
                                    {
                                        required: true,
                                        message: t("Please input your last name!"),
                                    },
                                ],
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label={t("Email")}>
                            {getFieldDecorator("email", {
                                rules: [
                                    {
                                        type: "email",
                                        message:
                                            t("The input is not valid Email!"),
                                    },
                                    {
                                        required: true,
                                        message: t("Please input your Email!"),
                                    },
                                ],
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label={t("Password")} hasFeedback>
                            {getFieldDecorator("password", {
                                rules: [
                                    {
                                        required: true,
                                        message: t("Please input your password!"),
                                    },
                                    {
                                        validator: this.validateToNextPassword,
                                    },
                                ],
                            })(<Input.Password />)}
                        </Form.Item>
                        <Form.Item label={t("Confirm Password")} hasFeedback>
                            {getFieldDecorator("confirm", {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            t("Please confirm your password!"),
                                    },
                                    {
                                        validator: this.compareToFirstPassword,
                                    },
                                ],
                            })(
                                <Input.Password
                                    onBlur={this.handleConfirmBlur}
                                />
                            )}
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            {getFieldDecorator("admin", {
                                valuePropName: "admin",
                            })(<Checkbox>{t('Admin')}</Checkbox>)}
                        </Form.Item>
                        <div className="button-group">
                            <div className="button-action">
                                <Button key="back" onClick={this.handleCancel}>
                                    {t('Cancel')}
                                </Button>
                            </div>
                            <div className="button-action">
                                <Button
                                    key="submit"
                                    type="primary"
                                    htmlType="submit"
                                    loading={this.state.loading}
                                    onClick={this.handleSubmit}
                                >
                                    {t('Submit')}
                                </Button>
                            </div>
                        </div>
                    </Form>
                </Modal>
            </div>
        );
    }
}

const BFormCreate = Form.create<UserFormProps>({ name: "register" })(
    RegistrationForm
);
const FormCreate = React.memo((props: any) => ChangeLanguage(BFormCreate,props));

export default FormCreate;
