import React from "react";
import {
    Switch,
    withRouter,
    RouteComponentProps,
    Link,
} from "react-router-dom";
import TableStatistic from "../statistic/table";
import ChartStatistic from "../statistic/chart";
import AccountMgt from "../account_mgt";
import "./style.scss";
import { PrivateRoute } from "redux/_components";
import ChangeLanguage from "language";
import { WithTranslation } from "react-i18next";

interface ContentToolsProps extends RouteComponentProps, WithTranslation {
    location: any;
    t: any;
}
class ContentToolsView extends React.PureComponent<ContentToolsProps, {}> {
    render() {
        const { t } = this.props;
        if (this.props.location.pathname === "/tools") {
            return (
                <div className="home-title">
                    <div>{t("Please select a feature")}.</div>
                    <Link to="/tools/table">
                        {t("Or click here to go to the Table statistic page")}.
                    </Link>
                </div>
            );
        }
        return (
            <div className="content">
                <Switch>
                    <PrivateRoute
                        path="/tools/table"
                        component={TableStatistic}
                    />
                    <PrivateRoute
                        path="/tools/chart"
                        component={ChartStatistic}
                    />
                    <PrivateRoute
                        path="/tools/account-mgt"
                        component={AccountMgt}
                    />
                </Switch>
            </div>
        );
    }
}
const ContentTools = React.memo(() =>
    ChangeLanguage(withRouter(ContentToolsView))
);
export default ContentTools;
