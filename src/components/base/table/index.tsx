import React, { PureComponent } from 'react';
import 'style/index.scss';
import './style.scss';

type BaseTableProps = {
    data?: {
        title?: {
            text?: string;
            action?: any;
        }
        header?: string[] | any;
        keyHeader?: string[];
        content?: {}[]
    }
}
export default class BaseTable extends PureComponent<BaseTableProps, {}> {
    render() {
        console.log('render:: BaseTable');
        const data = this.props.data;
        if (!data) { return <div className="base-table"></div> }
        return (
            <div className="base-table">
                {data.title && <div className="title-base-table">
                    <span className="title-base-table-text">{data.title.text}</span>
                    <span className="title-base-table-action" onClick={data.title.action && data.title.action.value}>{data.title.action && data.title.action.text}</span>
                </div>}
                <div className="body-base-table">
                    <div className="header-base-table d-flex">
                        {data.header && data.header.map((item: any, index: number) => {
                            return (
                                <div key={item} className={`item-header-base-table item-body-base-table-${data.keyHeader && data.keyHeader[index]}`}>
                                    {item}
                                </div>
                            );
                        })}
                    </div>
                    <div className="content-base-table">
                        {data.content && data.content.map((item: any, index: number) => {
                            return (
                                <div key={index} className="list-item-content-base-table d-flex">
                                    {data.keyHeader && data.keyHeader.map((itemHeader: any, index: number) => {
                                        return (
                                            <div key={itemHeader} className={`item-content-base-table item-body-base-table-${data.keyHeader && data.keyHeader[index]} item-body-base-table-${data.keyHeader && data.keyHeader[index]}-${item[itemHeader]}`}>
                                                {item[itemHeader]}
                                            </div>
                                        );
                                    })}
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export const BaseGridTalbe = (data: any) => <div className="base-grid-table"><BaseTable data={data} /></div>;