import React from "react";
import { connect } from "react-redux";
import { userActions } from "redux/_actions";
import { Form, Input, Icon, Button } from "antd";
import ChangeLanguage from "language";
import { FormComponentProps } from "antd/es/form";
import { WithTranslation } from "react-i18next";

import "./style.scss";

interface LoginProps extends WithTranslation, FormComponentProps {
    t: any;
    dispatch: any;
    form: any;
}

type LoginState = {};
class LoginForm extends React.PureComponent<LoginProps, LoginState> {
    constructor(props: any) {
        super(props);
        // reset login status
        this.props.dispatch(userActions.logout());
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        
        this.props.form.validateFields((error: any, values: any) => {
            if (!error) {
                console.log('login',values);
                const { username, password } = values;
                // server validate
                if (username && password) {
                    this.props.dispatch(userActions.login(username, password));
                }
            } else {
                console.log("error", error, values);
            }
        });
    };

    render() {
        console.log('render:: login page');
        const { getFieldDecorator } = this.props.form;
        const { t } = this.props;
        return (
            <div className="login-page">
                <br />
                <div className="login-head">
                    <h2 className="login-head-text">
                        {t("Welcome to the smart greenhouse")}
                    </h2>
                </div>
                <br />
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator("username", {
                            rules: [
                                {
                                    required: true,
                                    message: `${t(
                                        "Please input your username"
                                    )}!`,
                                },
                            ],
                        })(
                            <Input
                                prefix={
                                    <Icon
                                        type="user"
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                placeholder={t("Username")}
                                name="username"
                            />
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator("password", {
                            rules: [
                                {
                                    required: true,
                                    message: `${t(
                                        "Please input your Password"
                                    )}!`,
                                },
                            ],
                        })(
                            <Input
                                prefix={
                                    <Icon
                                        type="lock"
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                type="password"
                                placeholder={t("Password")}
                                name="password"
                            />
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            {t("Log in")}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

function mapStateToProps(state: any) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn,
    };
}
const LoginPageApp = Form.create({ name: "normal_login" })(LoginForm);

const connectedLoginPage = React.memo(() =>
    ChangeLanguage(connect(mapStateToProps)(LoginPageApp))
);
export { connectedLoginPage as LoginPage };
