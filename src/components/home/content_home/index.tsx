import React from "react";
import {
    Switch,
    RouteComponentProps,
    withRouter,
    Link,
} from "react-router-dom";
import { WithTranslation } from "react-i18next";

import SensorsNow from "../sensors_now";
import Controlling from "../controlling";
import Setting from "../setting";
import { PrivateRoute } from "redux/_components";

import "./style.scss";
import "style/index.scss";
import ChangeLanguage from "language";

interface ContentHomeProps extends RouteComponentProps, WithTranslation {
    location: any;
    t: any;
}
class ContentHomeView extends React.PureComponent<ContentHomeProps, {}> {
    render() {
        const { t } = this.props;
        if (this.props.location.pathname === "/home") {
            return (
                <div className="home-title">
                    <div>{t('Please select a feature')}.</div>
                    <Link to="/home/monitoring">
                       {t('Or click here to go to the Monitoring page')}.
                    </Link>
                </div>
            );
        }
        return (
            <div className="content">
                <Switch>
                    <PrivateRoute
                        path="/home/monitoring"
                        component={SensorsNow}
                    />
                    <PrivateRoute
                        path="/home/controlling"
                        component={Controlling}
                    />
                    <PrivateRoute path="/home/setting" component={Setting} />
                </Switch>
            </div>
        );
    }
}

const ContentHome = React.memo(() => ChangeLanguage(withRouter(ContentHomeView)));
export default ContentHome;
