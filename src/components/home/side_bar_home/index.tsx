import React from "react";
import { Layout, Menu, Icon } from "antd";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import ChangeLanguage from "language";
import { WithTranslation } from "react-i18next";

import "./style.scss";

const { Sider } = Layout;

interface SideBarHomeProps extends WithTranslation, RouteComponentProps {
    t: any;
    location: any;
}
class SideBarHomeComponent extends React.PureComponent<
    SideBarHomeProps,
    { collapsed: boolean }
> {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };
    render() {
        const { t, location } = this.props;
        return (
            <Sider theme="light" trigger={null} collapsible collapsed={this.state.collapsed}>
                <Menu mode="inline" selectedKeys={[location.pathname]}>
                    <Menu.Item>
                        <Icon
                            className="trigger"
                            type={
                                this.state.collapsed
                                    ? "menu-unfold"
                                    : "menu-fold"
                            }
                            onClick={this.toggle}
                        />
                    </Menu.Item>

                    <Menu.Item key="/home/monitoring">
                        <Link to="/home/monitoring">
                            <Icon type="desktop" className="monitoring" />
                            <span className="monitoring">
                                {t("Monitoring")}
                            </span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/home/controlling">
                        <Link to="/home/controlling">
                            <Icon type="control" className="controlling" />
                            <span className="controlling">
                                {t("Controlling")}
                            </span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/home/setting">
                        <Link to="/home/setting">
                            <Icon type="setting" className="setting" />
                            <span className="setting">{t("Setting")}</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        );
    }
}
const SideBarHome = React.memo(() => ChangeLanguage(withRouter(SideBarHomeComponent)));
export default SideBarHome;
