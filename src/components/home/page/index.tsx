import React from "react";
import SideBarHome from "../side_bar_home";
import ContentHome from "../content_home";
import { Redirect } from "react-router-dom";
import "./style.scss";
export default class Home extends React.PureComponent<{}, {}> {
    render() {
        return (
            <div className="layout">
                <Redirect to={{ pathname: "/home/monitoring" }} />
                <SideBarHome />
                <ContentHome />
            </div>
        );
    }
}
