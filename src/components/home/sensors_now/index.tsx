import React from "react";
import { AXIOS } from "access/axios";
import ChangeLanguage from "language";
import { notification } from "antd";
import { BaseGridTalbe } from "components/base/table";
import { WithTranslation } from "react-i18next";
import moment from "moment";

import "./style.scss";
import "style/index.scss";

interface SensorsNowStateProps extends WithTranslation {
    t: any;
}
type SensorsNowState = {
    id: string[];
    time: string[];
    temperature: string[];
    humidity: string[];
    light: string[];
    co2: string[];
    water_flow: string[];
};

class SensorsNowComponent extends React.PureComponent<
    SensorsNowStateProps,
    SensorsNowState
> {
    interval_id: any;

    constructor(props: any) {
        super(props);
        this.state = {
            id: [],
            time: [],
            temperature: [],
            humidity: [],
            light: [],
            co2: [],
            water_flow: [],
        };
    }

    componentDidMount() {
        const API_SENSORS_NOW = "api/sensors-now";
        const getData = async () => {
            try {
                const resp = await AXIOS.get(API_SENSORS_NOW);
                const data = resp.data;

                this.setState({
                    id: [data["0"]["id"], data["1"]["id"]],
                    time: [
                        moment(`${data["0"]["time"]}`).format(
                            "YYYY-MM-DD HH:mm:ss"
                        ),
                        moment(`${data["1"]["time"]}`).format(
                            "YYYY-MM-DD HH:mm:ss"
                        ),
                    ],
                    temperature: [
                        data["0"]["temperature"],
                        data["1"]["temperature"],
                    ],
                    humidity: [data["0"]["humidity"], data["1"]["humidity"]],
                    light: [data["0"]["light"], data["1"]["light"]],
                    co2: [data["0"]["co2"], data["1"]["co2"]],
                    water_flow: [
                        data["0"]["water_flow"],
                        data["1"]["water_flow"],
                    ],
                });
            } catch (error) {
                notification.error({
                    message: this.props.t("Error!"),
                    description: !error?.response?.data?.msg
                        ? this.props.t("Oops, something went wrong")
                        : this.props.t(error.response.data.msg),
                    onClick: () => {
                        console.log("Oops, something went wrong");
                    },
                });
            }
        };
        getData();
        this.interval_id = setInterval(() => {
            getData();
        }, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.interval_id);
        console.log("clearInterval");
    }

    render() {
        const { t } = this.props;
        const {
            id,
            time,
            temperature,
            humidity,
            light,
            co2,
            water_flow,
        } = this.state;
        const data = {
            title: {
                text: t("Sensors Now"),
            },
            header: [
                t("Index"),
                t("Time"),
                t("Temperature") + " (°C)",
                t("Humidity") + " (%RH)",
                t("Light") + " (lux)",
                // t("Concentration of CO") + 2 + " (ppm)",
                t("Water flow") + " (l/hour)",
            ],
            keyHeader: [
                "id",
                "time",
                "temperature",
                "humidity",
                "light",
                // "co2",
                "water_flow",
            ],
            content: id.map((value: any, index: number) => {
                return {
                    key: value,
                    id: value,
                    time: time[index],
                    temperature: temperature[index] !== null ? temperature[index] : "/",
                    humidity: humidity[index] !== null ? humidity[index]: "/",
                    light: light[index] !== null ? light[index] : "/",
                    // co2: co2[index] !== null ? co2[index] : "/",
                    water_flow:
                        water_flow[index] !== null ? water_flow[index] : "/",
                };
            }),
        };
        return (
            <div>
                <div className="table-sensors-now">{BaseGridTalbe(data)}</div>
            </div>
        );
    }
}
const SensorsNow = React.memo(() => ChangeLanguage(SensorsNowComponent));
export default SensorsNow;
