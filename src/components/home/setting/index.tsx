import React from 'react';
import { Radio } from 'antd';
import ChangeLanguage from 'language';
import { WithTranslation } from 'react-i18next';

import './style.scss';
import 'style/index.scss';

interface SettingProps extends WithTranslation {
    t: any;
    i18n: any;
}
class SettingComponent extends React.PureComponent<SettingProps, { language: string }> {
    constructor(props: any) {
        super(props);
        this.state = {
            language: ''
        };
    }
    onChange = (e: any) => {
        this.setState({ language: e.target.value })
        console.log('onchange:', e.target.value);
    }
    changeLanguage = (lng: string) => {
        this.props.i18n.changeLanguage(lng);
    };
    render() {
        const { t, i18n } = this.props;
        
        const languageCurrent = i18n.language || window.localStorage.i18nextLng
        console.log('current', languageCurrent);
        return (
            <div className="setting-page">
                <div className="language">
                    <span className="title" >{t('Language')}:</span>
                    <Radio.Group name="radiogroup" defaultValue={languageCurrent} onChange={this.onChange}>
                        <Radio value={'en'} onClick={() => this.changeLanguage('en')} >{t('English')}</Radio>
                        <Radio value={'vi'} onClick={() => this.changeLanguage('vi')} >{t('Vietnamese')}</Radio>
                    </Radio.Group>
                </div>
            </div>
        );
    }
}

const Setting = React.memo(() => ChangeLanguage(SettingComponent));
export default Setting;