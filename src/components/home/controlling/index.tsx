import React from "react";
import { Switch, Button, Popconfirm, notification } from "antd";
import ChangeLanguage from "language";
import { AXIOS } from "access/axios";

import "./style.scss";
import "style/index.scss";

type TypeOfSwitch = {
    isLock: boolean;
    switches: Record<string, boolean>;
    SWITCH_ID: Array<string>;
    loading: boolean;
};

class ControllingComponent extends React.PureComponent<any, TypeOfSwitch> {
    constructor(props: any) {
        super(props);
        this.state = {
            isLock: true,
            switches: {},
            SWITCH_ID: [],
            loading: false,
        };
    }

    async componentDidMount() {
        const API_CONTROL_SWITCH = "api/switch-control";
        try {
            this.setState({ loading: true });
            const resp = await AXIOS.get(API_CONTROL_SWITCH);
            const data = resp.data;
            const switches = {} as any;
            const SWITCH_ID = data.map((item: any, index: number) => {
                switches[`relay-${index}`] = Boolean(item.status);
                return `relay-${index}`;
            });
            this.setState({ SWITCH_ID: SWITCH_ID, switches: switches });
        } catch (error) {
            console.log("err", error);
            notification.error({
                message: this.props.t("Error!"),
                description: !error?.response?.data?.msg
                    ? this.props.t("Oops, something went wrong")
                    : this.props.t(error.response.data.msg),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
        } finally {
            this.setState({ loading: false });
        }
    }

    onChange = (key: string) => {
        try {
            !this.state.switches[key]
                ? AXIOS.post(`control/switch-0/${key}/on`)
                : AXIOS.post(`control/switch-0/${key}/off`);
            this.setState((prev: any) => {
                return {
                    ...prev,
                    switches: {
                        ...prev.switches,
                        [key]: !this.state.switches[key],
                    },
                    isLock: true,
                };
            });
            notification.success({
                message: this.props.t("Successfully!"),
                description: `${this.props.t("Relay")} ${key[key.length - 1]} ${
                    !this.state.switches[key]
                        ? this.props.t("is on")
                        : this.props.t("is off")
                }.`,
                onClick: () => {
                    console.log("Switch change");
                },
            });
        } catch (err) {
            console.log("err", err);
            notification.error({
                message: this.props.t("Error!"),
                description: this.props.t("Oops, something went wrong"),
                onClick: () => {
                    console.log("Oops, something went wrong");
                },
            });
        }
    };

    toggleCancel = () => {
        this.setState({ isLock: true });
    };
    toggleConfirm = () => {
        this.setState({ isLock: false });
    };

    renderToggle = () => {
        const { t } = this.props;
        return (
            <div>
                <Popconfirm
                    placement="topLeft"
                    title={t("Change status switch")}
                    onConfirm={() => this.toggleConfirm()}
                    onCancel={() => this.toggleCancel()}
                    okText={t("Unlock Switch")}
                    cancelText={t("Lock Switch")}
                >
                    <Button type="primary">
                        {this.state.isLock
                            ? t("Locked Switch")
                            : t("Unlocked Switch")}
                    </Button>
                </Popconfirm>
                <br />
                <br />
            </div>
        );
    };

    renderSwitch = () => {
        const { t } = this.props;
        return this.state.SWITCH_ID.map((item: any, index: number) => {
            return (
                <div key={item} className="item-switch">
                    <div className="item-switch-name">
                        {`${t("Relay")} ${index}`}
                    </div>
                    <Switch
                        disabled={this.state.isLock}
                        onChange={() => this.onChange(item)}
                        checked={this.state.switches[item]}
                    />
                    <br />
                    <br />
                </div>
            );
        });
    };
    render() {
        console.log("render:: Controlling");
        return (
            <div>
                {this.renderToggle()}
                {this.renderSwitch()}
            </div>
        );
    }
}

const Controlling = React.memo(() => ChangeLanguage(ControllingComponent));
export default Controlling;
